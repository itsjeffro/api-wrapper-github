<?php

namespace Itsjeffro\Github\Resources\Repositories;

use Itsjeffro\Github\Api;

class Commits extends Api
{
    /**
     * Return all commits from repository.
     *
     * @param  string $owner
     * @param  string $repo
     * @param  array $params
     * @return object
     */
    public function all($owner, $repo, $params = [])
    {
        $this->setEndpoint('repos/' . $owner . '/' . $repo . '/commits');

        return $this->request('GET', $this->getEndpoint(), [
                'query' => $params,
            ]);
    }

    /**
     * Return information from commit.
     *
     * @param  string $owner
     * @param  string $repo
     * @param  string $sha
     * @return object
     */
    public function get($owner, $repo, $sha)
    {
        $this->setEndpoint('repos/' . $owner . '/' . $repo . '/' . $sha);

        return $this->request('GET', $this->getEndpoint());
    }
}
