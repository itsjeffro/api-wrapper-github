<?php

namespace Itsjeffro\Github\Resources\Repositories;

use Itsjeffro\Github\Api;

class Branches extends Api
{
    /**
     * Return all branches from repository.
     *
     * @param  string $owner
     * @param  string $repo
     * @return object
     */
    public function all($owner, $repo)
    {
        $this->setEndpoint('repos/' . $owner . '/' . $repo . '/branches');

        return $this->request('GET', $this->getEndpoint());
    }

    /**
     * Return information from branch.
     *
     * @param  string $owner
     * @param  string $repo
     * @param  string $branch
     * @return object
     */
    public function get($owner, $repo, $branch)
    {
        $this->setEndpoint('repos/' . $owner . '/' . $repo . '/' . $branch);

        return $this->request('GET', $this->getEndpoint());
    }
}
