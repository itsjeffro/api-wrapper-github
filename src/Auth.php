<?php

namespace Itsjeffro\Github;

use Itsjeffro\Http\Client as HttpClient;

class Auth
{
    const BASE_URI = 'https://github.com/login/oauth';
    
    /**
     * @param string
     */
    private $clientId;
    
    /**
     * @param string
     */
    private $clientSecret;
    
    /**
     * Auth constructor
     *
     * @return void
     */  
    public function __construct(array $config = [])
    {
        $this->clientId = isset($config['client_id']) ? $config['client_id'] : '';

        $this->clientSecret = isset($config['client_secret']) ? $config['client_secret'] : '';
    }
    
    /**
     * Get auth base uri.
     *
     * @return void
     */ 
    public function getBaseUri()
    { 
        return self::BASE_URI;
    }
    
    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }
    
    /**
     * @return string
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }
    
    /**
     * Return URI to use to authorize a user.
     *
     * @param array $options
     * @return string
     */
    public function authorize($options = [])
    {
        $scope = isset($options['scope']) ? implode(',', $options['scope']) : '';
        
        $query = [
            'scope'     => $scope,
            'client_id' => $this->getClientId(),
        ];
        
        return $this->getBaseUri() . '/authorize?' . urldecode(http_build_query($query));
    }
    
    /**
     * Return URI to use to authorize a user.
     *
     * @param string $code
     * @return string
     */
    public function callback($code)
    {
        $params = [
            'client_id' => $this->getClientId(),
            'client_secret' => $this->getClientSecret(),
            'code' => $code,
        ];
    
        $http = new HttpClient();      
        
        $response = $http->request('POST', $this->getBaseUri() . '/access_token', [
            'headers' => [
                'Accept' => 'application/json',
            ],          
            'form_params' => $params,
        ]);
        
        return $response;
    }
}