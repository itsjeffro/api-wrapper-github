<?php

namespace Itsjeffro\Github;

use GuzzleHttp\Client;

class Api
{
    const API_BASE_URI = 'https://api.github.com';

    const API_VERSION = '3.0';
    
    /**
     * @var string
     */
    private $endpoint;

    /**
     * Get api base uri.
     *
     * @return void
     */ 
    public function getBaseUri()
    { 
        return self::API_BASE_URI;
    }

    /**
     * Return full url of api being requested.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->getBaseUri() . '/' . $this->endpoint;
    }

    /**
     * Set the endpoint.
     *
     * @param string $endpoint
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = ltrim($endpoint, '/');
    }

    /**
     * Get endpoint.
     *
     * @param string $endpoint
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @param string $method
     * @param string $endpoint
     * @param array  $options
     */
    public function request($method, $endpoint, $options = []) 
    {
        $options['headers'] = [
            'Accept' => 'application/json',
        ];

        $client = new Client([
            'base_uri' => $this->getBaseUri(),
        ]);

        return $client->request($method, $endpoint, $options);
    }
}
    