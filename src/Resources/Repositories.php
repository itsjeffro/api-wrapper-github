<?php

namespace Itsjeffro\Github\Resources;

use Itsjeffro\Github\Api;
use Itsjeffro\Github\Resources\Repositories\Branches;
use Itsjeffro\Github\Resources\Repositories\Commits;

class Repositories extends Api
{
    /**
     * @var string
     */
    private $from;

    /**
     * Return all repositories or from an owner.
     *
     * @param  string $owner
     * @param  string $repo
     * @return object
     */
    public function all($owner, $repo = null)
    {
        if ($owner && empty($repo)) {
            $this->setEndpoint('users/' . $owner . '/repos');
        } 
        else {
            $this->setEndpoint('repositories');
        }

        return $this->request('GET', $this->getEndpoint());
    }

    /**
     * Return information from repository.
     *
     * @param  string $owner
     * @param  string $repo
     * @return object
     */
    public function get($owner, $repo)
    {
        $this->setEndpoint('repos/' . $owner . '/' . $repo);

        return $this->request('GET', $this->getEndpoint(). ($this->from ? '/' . $this->from : ''));
    }

    /**
     * Return branches from repository.
     *
     * @return object
     */
    public function branches()
    {
        $branches = new Branches;
        return $branches;
    }

    /**
     * Return commits from repository.
     *
     * @return object
     */
    public function commits()
    {
        $commits = new Commits;
        return $commits;
    }

    /**
     * List languages from contributors.
     * 
     * @return self
     */
    public function contributors()
    {
        $this->from = 'contributors';
        return $this;
    }

    /**
     * List languages from repository.
     * 
     * @return self
     */
    public function languages()
    {
        $this->from = 'languages';
        return $this;
    }

    /**
     * List tags from repository.
     * 
     * @return self
     */
    public function tags()
    {
        $this->from = 'tags';
        return $this;
    }

    /**
     * List teams from repository.
     * 
     * @return self
     */
    public function teams()
    {
        $this->from = 'teams';
        return $this;
    }
}
