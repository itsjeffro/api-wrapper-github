##Introduction
This api wrapper uses the endpoints from v3 of the Github API.

https://developer.github.com/v3/

##Install
```
$ composer install
```

##Include alias
To be able to use the main client to access the endpoints, first include an alias to the client.

```
<?php

use Itsjeffro\Github\Client;

$github = new Client;
```

##Endpoint examples
###Repositories
```
<?php

$response = $github
	->repositories()
	->all('owner', 'repo');

$data = json_decode($response->getBody());
```

#####Available methods
You can chain the following methods to the main resource method. More available methods for each endpoint resource can be viewed in the Resources directory.

```
<?php

$response = $github
	->repositories()
	->commits()
	->all('owner', 'repo', ['sha' => 'master']);
```
```
/**
 * @return self 
 */
->branches()

/**
 * @return self
 */
->commits()
```