<?php

namespace Itsjeffro\Github;

use Itsjeffro\Github\Resources\Repositories;

class Client extends Api
{   
    /**
     * Return repositories or repository from user.
     *
     * @param string $owner
     * @param string $repo
     * @return array
     */ 
    public function repositories()
    {
        $repositories = new Repositories();
        return $repositories;
    } 
}